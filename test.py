from vertex import Vertex
from pprint import pprint




def test_with_login(host,username, password):
    print("====test_login===") 
    with Vertex(host, username, password) as cl:
        print(cl.login())
        print("logged in")

    
def test_with_backofficelogin(host,username, password):
    print("====test_backofficelogin===") 
    with Vertex(host, username, password) as cl:
        print(cl.backofficelogin())

def test_with_get_accounts(host,username, password):
    print("====test_get_accounts===") 
    with Vertex(host, username, password) as cl:
        print("accounts\n\n")
        accs = cl.get_accounts()
        pprint(accs)

def test_with_get_account_summary(host,username, password):
    print("====test_get_accountsummary===") 
    with Vertex(host, username, password) as cl:
        print("accounts\n\n")
        accs = cl.get_accounts()
        
        for acc in accs:
            accid = acc["AccountID"]
            print(cl.get_accounts_summary(accid))
            # pprint(acc)

def test_with_get_new_tick(host,username, password):
    print("====test_get_new_tick===") 
    with Vertex(host, username, password) as cl:
        pprint(cl.get_new_tick())



if __name__ == "__main__":
    import os
    host = 'http://173.249.52.116/webtrader/WebService.svc'
    username = os.getenv("VERTEX_USERNAME", "Testapp")
    password = os.getenv("VERTEX_PASSWORD")
    if not password:
        print("please set VERTEX_PASSWORD")
        exit(2)
    test_with_login(host,username, password)
    test_with_backofficelogin(host,username, password)
    test_with_get_accounts(host,username, password)
    test_with_get_account_summary(host,username, password)
    test_with_get_new_tick(host,username, password)
