import requests
import json
from . import errors

def to_bool_or_false(x):
    try:
        return bool(x)
    except:
        return False

def to_int_or_0(x):
    try:
        return int(x)
    except:
        return 0


validators = {
    'num': lambda x:  int(x) and x.isdigit(),
    'valid_userid': lambda x: x.isdigit() and int(x)>0,
    "bool": lambda x: bool(int(x)),
    "int": lambda x: 0 if not x else int(x)
}

validation_rules = {
    "VersionInfo.Rel": validators['num'],
    "sessionid":  validators['num'],
    "VersionInfo.Rel": validators['num'],
    "VersionInfo.Ver": validators['num'],
    "VersionInfo.Patch": validators['num'],
    "VersionInfo.ForceUpdate": validators['num'],
    "VersionInfo.UpdateType": validators['num'],
    "IsReadOnly": validators["bool"],
    "DemoClient": validators["bool"],
    "EnableNews": validators["bool"],

}

conversion_rules = {
    "VersionInfo.Rel": to_int_or_0,
    "VersionInfo.Ver": to_int_or_0,
    "VersionInfo.Patch": to_int_or_0,
    "VersionInfo.ForceUpdate": to_int_or_0,
    "VersionInfo.UpdateType": to_int_or_0,
    "DepId": to_int_or_0,
    "CommCalcType": to_int_or_0,
    "ClientType": to_int_or_0,
    "DemoClient": to_int_or_0,
    "IsReadOnly": to_int_or_0,
    "Sms": to_int_or_0,
    "EnableNews":to_int_or_0,
}

class DemoClientType:
    RealClient = 0
    DemoClient = 1


class ClientType:
    Client = 1
    Office = 2
    Group  = 3

class UserType:
    DealerUser = 1
    ClientUser = 2
    DemoClientUser = 3


class CommCalcType:
    OpenType = 1
    CloseType = 2


class AccountType:
    DemoAccountType = 0
    NormalAccountType = 1
    CoverageAccount = 2


class GetAccountSummaryResp:
    def __init__(self):
        self.account_id = None
        self.account_type = None
        self.is_marging = None
        self.demo_account = None
        self.lock_liquidate = None





def get_error_string(errval):
    for m in dir(errors):
        res = getattr(errors, m)
        # print(m,  res, errval)
        if res == errval:
            return m
    return ""

def raise_if_error(data):
    if isinstance(data, int):
        errorstring = get_error_string(data)
        if errorstring:
            raise ValueError("raised error with value: {} original is {}".format(data, errorstring))
        else:
            raise ValueError("raised error with value: {} and couldn't find it in errors.".format(data))

    if not (isinstance(data, dict) or isinstance(data, list)):
        raise ValueError("data {} should be dict not {}".format(data, type(data)))

    return data


def clean_data(data):
    cleaned_data_dict = {}
    cleaned_data_list = []
    if isinstance(data, dict):
        for k, v in data.items():
            if k in conversion_rules:
                f = conversion_rules[k]
                cleaned_data_dict[k] = f(v)
            else:
                cleaned_data_dict[k] = v
    elif isinstance(data, list):
        for el in data:
            cleaned_data_list.append(clean_data(el))

    if isinstance(data, dict):
        return cleaned_data_dict
    elif isinstance(data, list):
        return cleaned_data_list
    else:
        return data

def raise_if_invalid_user_id(user_id):
    raise_if_error(user_id)

def is_valid_user_id(user_id):
    try:
        raise_if_error(user_id)
        return True
    except:
        return False



ENDPOINTS = {
    'login': 'Login',
    'getaccounts': 'GetAccounts',
    'backofficelogin': 'BackofficeLogin',
    'getaccountsummary': 'GetAccountSumm',
    'newposition': 'NewPosition',
    'getnewtick': 'GetNewTick',
}
class Vertex:
    def __init__(self, host, username, password):
        self.host = host
        self.username = username
        self.password = password
        self.session = requests.Session()

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass


    def _mkrequest(self, endpoint, params):
        resp = self.session.get(endpoint, params=params)
        resp.raise_for_status()

        resp_json = resp.json()

        data_str = resp_json['d']

        data = json.loads(data_str)
        cleaned_data = clean_data(data)

        return cleaned_data

    def login(self):
        endpoint = ENDPOINTS['login']
        url = "{}/{}".format(self.host, endpoint)
        params = {"username":self.username, "password":self.password}
        return self._mkrequest(url, params)

    def backofficelogin(self):
        endpoint = ENDPOINTS['backofficelogin']
        url = "{}/{}".format(self.host, endpoint)
        params = {"username":self.username, "password":self.password}
        return self._mkrequest(url, params)

    def get_accounts(self):

        endpoint = ENDPOINTS['getaccounts']
        url = "{}/{}".format(self.host, endpoint)
        params = {}
        return self._mkrequest(url, params)


    def get_accounts_summary(self, account_id):
        endpoint = ENDPOINTS['getaccountsummary']
        url = "{}/{}".format(self.host, endpoint)
        params = {}
        return self._mkrequest(url, params)

    def new_position(self, account_id=None, buy_or_sell=None, amount=None, symbol_id=None, price=None, note="", user_defined_date="DD/MM/yyyy HH:mm:ss"):
        """
            account_id: valid account id
            buy_or_sell : 1 buy, -1 sell
            amount: long	
            symbol_id: int
            price: int
            note: str
            user_defined_data: str

        """
        endpoint = ENDPOINTS['newposition']
        url = "{}/{}".format(self.host, endpoint)

        params = {"AccountID": account_id, "BuySell": buy_or_sell, "Amount":amount, "SymbolID": symbol_id, "Price":price, "note":note, "UserDefinedData":user_defined_date}
        return self._mkrequest(url, params)

    def get_new_tick(self):
        endpoint = ENDPOINTS['getnewtick']
        url = "{}/{}".format(self.host, endpoint)
        params = {}
        return self._mkrequest(url, params)

    def deliver_symbol(self):
        raise NotImplementedError()

    def new_order(self):
        raise NotImplementedError()
    
